#!/usr/bin/node

var PROCESS = require('child_process');
var HTTP = require('http');
var URL = require('url');
var CHEERIO = require('cheerio');
var REQUEST = require('request');
var Events = require('events');

var EventType = {
    BeginProcess: "beginEvent",
    IdvEvent: "idvEvent",
    PlayerObjectId: "playerEvent",
    NumberObjectId: "numberEvent",
    SpawnVlc: "SpawnVlcEvent",
    EndRequest: "endEvent",
    ErrorEvent: "errorEvent"

};

function log(val) {
    console.log(val);
};

function VideoGrabber() {

    Events.EventEmitter.call(this);

    var res = null;
    var self = this;

    var CONSTANTS = {
        tvpVideoInfo: "http://www.tvp.pl/pub/stat/videofileinfo?video_id=",
        kabaretLoadSkecz: "http://kabaret.tworzymyhistorie.pl/inc/load_skecz.php?i=",
        initParams: "object param[name='InitParams']",
        kabaretIframe: "iframe[src*='tvp.pl']",
        kabaretMainIDVparam: "input[name='idv']",
        stateOK: "OK",
        stateERROR: "ERROR"
    };

    var state = CONSTANTS.stateOK;

    function getState() {
        return state;
    };

    this.getAddressParam = function (request) {
        var urlInfo = URL.parse(request.url, true);
        return urlInfo.query.address;
    };

    function getIdvParamFormAddress(url) {
        log("RUN request");
        REQUEST(url, function (err, response, body) {
            if (!err && response.statusCode == 200) {
                $ = CHEERIO.load(body);
                var idv = $(CONSTANTS.kabaretMainIDVparam).val();
                log("idv: " + idv);

                self.emit(EventType.IdvEvent, idv);
            } else {
                self.emit(EventType.ErrorEvent, err);
            }
        });
    };

    function getTvpObjectIdFromURL(tvpUrl) {
        var regex = RegExp('.*-([0-9]+)/player/');
        var match = regex.exec(tvpUrl);

        return match[1];
    };

    function getObjectIdFromPageSkecz(idv) {
        var url = CONSTANTS.kabaretLoadSkecz + idv;
        log("Getting: " + url);
        REQUEST(url, function (err, response, body) {
            if (!err && response.statusCode == 200) {
                $ = CHEERIO.load(body);
                var tvpUrl = $(CONSTANTS.kabaretIframe).attr('src');
                log("tvpUrl: " + tvpUrl);
                var objectId = getTvpObjectIdFromURL(tvpUrl);
                log("objectId: " + objectId);

                if (objectId == "player") {
                    self.emit(EventType.PlayerObjectId, tvpUrl);
                } else {
                    self.emit(EventType.NumberObjectId, objectId);
                }
            } else {
                self.emit(EventType.ErrorEvent, err);
            }
        });
    };


    function processTvpVideoInfo(objectId) {
        var urlInfo = CONSTANTS.tvpVideoInfo + objectId;
        REQUEST(urlInfo, function (err, response, body) {
            if (!err && response.statusCode == 200) {
                log("Getting: " + urlInfo);
                var json = JSON.parse(body);
                log("VideoUrl: " + json.video_url);

                self.emit(EventType.SpawnVlc, json.video_url);
            } else {
                self.emit(EventType.ErrorEvent, err);
            }
        });
    };

    function spawnVLC(videoUrl) {
        PROCESS.spawn("vlc", [videoUrl], {detached: true});

        self.emit(EventType.EndRequest, "end");
    };


    function processFrameContent(iframeUrl) {
        log("Looking for objectId in body...");
        log("Getting iframeUrl: " + iframeUrl);
        REQUEST(iframeUrl, function (err, response, body, next) {
            if (!err && response.statusCode == 200) {
                $ = CHEERIO.load(body);
                var tvpUrl = $(CONSTANTS.initParams).attr("value");
                var arr = tvpUrl.split(",");
                var objectId = "xxxxx";
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].indexOf("video_id") == 0) {
                        objectId = arr[i].split("=")[1];
                        break;
                    }
                }
                log("video_id param: " + objectId);

                self.emit(EventType.NumberObjectId, objectId);
            } else {
                self.emit(EventType.ErrorEvent, err);
            }
            ;
        });
    };

    this.process = function (url, response) {
        res = response;
        self.emit(EventType.BeginProcess, url);
    };

    this.listen = function (evt, callback) {
        self.on(evt, function () {
            log("====> EVENT: " + evt);
            callback.apply(this, arguments);
        });
    };

    this.listen(EventType.BeginProcess, getIdvParamFormAddress);

    this.listen(EventType.IdvEvent, getObjectIdFromPageSkecz);

    this.listen(EventType.NumberObjectId, processTvpVideoInfo);

    this.listen(EventType.SpawnVlc, spawnVLC);

    this.listen(EventType.PlayerObjectId, processFrameContent);
};

VideoGrabber.prototype.__proto__ = Events.EventEmitter.prototype;

HTTP.createServer(function (req, res) {

    var vg = new VideoGrabber();
    var url = vg.getAddressParam(req);

    vg.on(EventType.EndRequest, function () {
        log("====> EVENT: " + EventType.EndRequest);
        res.writeHead(200, {'Content-Type': 'text/html'});

        res.write("ok: ");

        log("DONE");
        res.end();
    });

    vg.on(EventType.ErrorEvent, function (error) {
        log("====> EVENT: " + EventType.ErrorEvent);
        res.writeHead(500, {'Content-Type': 'application/json'});

        res.write(JSON.stringify(error));

        log("ERROR: " + JSON.stringify(error));
        res.end();
    });

    vg.process(url, res);

}).listen(9000);